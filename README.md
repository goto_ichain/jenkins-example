# jenkins-example

## 動作確認方法

### git clone する

```shell
git clone {repo}
```

### スクリプトのモードを変更する

```shell
chmod +x jenkins/scripts/hello.sh
chmod +x localstack/ddb/ready.sh
```

### jenkins, mysql, localstack を起動する

```shell
docker compose up -d
```

### jenkins の初期パスワードを確認する

```shell
docker compose logs jenkins
```

### jenkins にブラウザからアクセスする

http://localhost:8080 にアクセスし、初期パスワードを入力する  
プラグインは Dockerfile で設定済みなので、プラグインインストールのページを閉じる

### test ジョブの作成

ジョブの追加で Pipeline を選択し、test という名前のジョブを作成する  
`jenkinsfiles/job.groovy` を Pipeline にコピーする

ジョブの追加で Pipeline を選択し、handler という名前のジョブを作成する  
`jenkinsfiles/handler.groovy` を Pipeline にコピーする

### ジョブを実行する

ブラウザから handler を選びビルド実行する
