def JOB_ID = UUID.randomUUID().toString();
pipeline {
    agent any
    stages {
        // stage 1, stage 2, stage 3 の直列実行
        stage('stage 1') {
            steps {
                build job: 'test', parameters: [
                    [$class: 'StringParameterValue', name: 'JOB_ID', value: JOB_ID]
                ]
            }
        }
        stage('stage 2') {
            // parallel 1, parallel 2, parallel 3 の並列実行
            parallel {
                stage('parallel 1') {
                    steps {
                        build job: 'test', parameters: [
                            [$class: 'StringParameterValue', name: 'JOB_ID', value: JOB_ID]
                        ]
                    }
                }
                stage('parallel 2') {
                    steps {
                        build job: 'test', parameters: [
                            [$class: 'StringParameterValue', name: 'JOB_ID', value: JOB_ID]
                        ]
                    }
                }
                stage('parallel 3') {
                    steps {
                        build job: 'test', parameters: [
                            [$class: 'StringParameterValue', name: 'JOB_ID', value: JOB_ID]
                        ]
                    }
                }
            }
        }
        stage('stage 3') {
            steps {
                build job: 'test', parameters: [
                    [$class: 'StringParameterValue', name: 'JOB_ID', value: JOB_ID]
                ]
            }
        }
    }
}
