pipeline {
    agent any
    stages {
        stage('job') {
            steps {
                // shell script の実行
                sh '/usr/share/jenkins/ref/scripts/hello.sh'
                // mysql の実行
                sh 'mysql -u $MYSQL_USER -p$MYSQL_PASSWORD -h $MYSQL_HOST < /usr/share/jenkins/ref/scripts/select_test_table.sql'
                // awscli の実行
                sh 'aws --endpoint-url=http://localstack:4566 dynamodb list-tables'
            }
        }
    }
}
