#!/bin/bash

set -e

aws --endpoint-url=http://localhost:4566 dynamodb create-table \
    --table-name test-history \
    --attribute-definitions \
        AttributeName=TestId,AttributeType=S \
    --key-schema \
        AttributeName=TestId,KeyType=HASH \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --table-class STANDARD

aws --endpoint-url=http://localhost:4566 dynamodb list-tables
