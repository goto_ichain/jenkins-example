USE jenkins_example;

CREATE TABLE test_table(
  id INT unsigned AUTO_INCREMENT NOT NULL,
  name VARCHAR(32) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
);

INSERT INTO test_table (name) VALUES
('name1'),
('name2'),
('name3'),
('name4'),
('name5');
